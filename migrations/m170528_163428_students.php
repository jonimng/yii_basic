<?php

use yii\db\Migration;

class m170528_163428_students extends Migration
{
    public function up()
    {
	$this->createTable('students', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
			'id_num' => $this->string()->notNull(),
			'age' => $this->integer()->notNull(),
               ]);

    }

    public function down()
    {
		$this->dropTable('students');
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}