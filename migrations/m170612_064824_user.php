<?php

use yii\db\Migration;

class m170612_064824_user extends Migration
{
    public function up()
    {

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'auth_Key' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
               ]);
    }

    public function down()
    {
       $this->dropTable('user');

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
