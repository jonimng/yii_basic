<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'View Student';
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1>All Records via Show Action</h1>
    <?php foreach ($student as $student): ?>
	    <p>
	    	<b>Name:</b> <?= $student->name ?>
	    	<br />
	    	<b>ID:</b><?= $student->id_num ?>
	    	<br />
	    	<b>Age:</b><?= $student->age ?>
	    	<br />    	
	    </p>
    <?php endforeach; ?>
</div>
