<?php

use yii\helpers\Html;

?>
<div class="site-about">
    <h1>Single Record via Index Action</h1>

    <p>
    	<b>Name:</b> <?= $student->name ?>
    	<br />
    	<b>ID:</b><?= $student->id_num ?>
    	<br />
    	<b>Age:</b><?= $student->age ?>
    	<br />    	
    </p>
</div>